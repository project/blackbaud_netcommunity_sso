                                                                                                                                       
                                                                     
                                             
--------------------------------------------------------------


This module allows user to have single sign on from black baud netcommunity.



Installation
-------------

 * Copy the whole "blackbaud_netcommuniy_sso" directory to your modules directory - "sites/all/modules" and
   enable the module from path "admin/modules".


Usage
-----
   
 * Go to "admin/config/content/blackbaud". Please enter the secret key and site url provided by blackbaud netcommunity.and click on save configurations.
